# Ansible Vault

You will often not want to have variables in these files in cleartext.  As an example, you might need to put passwords in your variable files.  This is made very simple in the Ansible Automation platform, where you can create different types of credentials in encrypted form so that they are able to be used but not decrypted. Sometimes when you are creating automation however you still want to use files and so still need a way to encrypt specific variables or entire files.  This is where `ansible-vault` is useful.

There are a number of ways to use `ansible-vault`, but here we are going to focus on two aspects:
 * encrypting a string
 * using that encrypted string in a variable file


## Encrypt a string

To encrypt a string, you will use the `ansible-vault` command with the encrypt_string argument.

```bash
ansible-vault encrypt_string
```
This will ask you to input and confirm a vault password, and then will have you type in the text you want encrypted.  
<!-- {% raw %} -->

```sh
New Vault password: 
Confirm New Vault password: 
Reading plaintext input from stdin. (ctrl-d to end input, twice if your content does not already have a newline)
mypassword
Encryption successful
!vault |
          $ANSIBLE_VAULT;1.1;AES256
          63666164616466333764396665653237376235613838613665633639366564613536306339633365
          6433636635363265343139383535316536353934373762370a346663623634626232353765393130
          36313063343338323834393962373863653363326362313735353737373935363961323963323232
          3731653334363166660a356237346539336637326362653434353937386330306666366464666533
          3738
```
<!-- {% endraw %} -->

## Use an Encrypted String

When you are done, you can add the text from `!vault` onward to a variable file.  In this example we will add the variable to a vars.yml file.

```bash
vi vars.yml
```
<!-- {% raw %} -->

```yml
mypasswd: vault |
          $ANSIBLE_VAULT;1.1;AES256
          63666164616466333764396665653237376235613838613665633639366564613536306339633365
          6433636635363265343139383535316536353934373762370a346663623634626232353765393130
          36313063343338323834393962373863653363326362313735353737373935363961323963323232
          3731653334363166660a356237346539336637326362653434353937386330306666366464666533
          3738
```
<!-- {% endraw %} -->


## Challenge

You can use this variable in your playbooks, though these is some additional work to use this with ansible-navigator.

Read the doc on setting up a vault password file, and then use the vault password in a playbook.
https://ansible.readthedocs.io/projects/navigator/faq/#how-can-i-use-a-vault-password-with-ansible-navigator


For more information on using Ansible Vault, you can go to the ansible documentation, and also in the Red Hat Ansible for Network Automation online class. \
https://docs.ansible.com/ansible/latest/vault_guide/index.html \
https://www.redhat.com/en/services/training/do457-ansible-network-automation
